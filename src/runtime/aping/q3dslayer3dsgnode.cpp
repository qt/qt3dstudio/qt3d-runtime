/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dslayer3dsgnode_p.h"
#include <QOpenGLContext>
#include <QOpenGLFunctions>

#ifndef GL_TEXTURE_2D_MULTISAMPLE
#define GL_TEXTURE_2D_MULTISAMPLE         0x9100
#endif

QT_BEGIN_NAMESPACE

static QSGMaterialType materialType;
static QSGMaterialType msaa2MaterialType;
static QSGMaterialType msaa4MaterialType;

const char * const *q3ds_sg_vs_attributes()
{
    static char const *const attr[] = { "qt_VertexPosition", "qt_VertexTexCoord", 0 };
    return attr;
}

static inline const char *q3ds_sg_vs()
{
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    if (ctx->format().version() >= qMakePair(3, 2) && ctx->format().profile() == QSurfaceFormat::CoreProfile) {
        return ""
               "#version 150 core                                   \n"
               "uniform mat4 qt_Matrix;                             \n"
               "in vec4 qt_VertexPosition;                          \n"
               "in vec2 qt_VertexTexCoord;                          \n"
               "out vec2 qt_TexCoord;                               \n"
               "void main() {                                       \n"
               "   qt_TexCoord = qt_VertexTexCoord;                 \n"
               "   gl_Position = qt_Matrix * qt_VertexPosition;     \n"
               "}";
    } else {
        return ""
               "uniform highp mat4 qt_Matrix;                       \n"
               "attribute highp vec4 qt_VertexPosition;             \n"
               "attribute highp vec2 qt_VertexTexCoord;             \n"
               "varying highp vec2 qt_TexCoord;                     \n"
               "void main() {                                       \n"
               "   qt_TexCoord = qt_VertexTexCoord;                 \n"
               "   gl_Position = qt_Matrix * qt_VertexPosition;     \n"
               "}";
    }
}

const char * const *Q3DSLayer3DSGMaterialShader::attributeNames() const
{
    return q3ds_sg_vs_attributes();
}

const char *Q3DSLayer3DSGMaterialShader::vertexShader() const
{
    return q3ds_sg_vs();
}

const char *Q3DSLayer3DSGMaterialShader::fragmentShader() const
{
    // Make the result have pre-multiplied alpha.
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    if (ctx->format().version() >= qMakePair(3, 2) && ctx->format().profile() == QSurfaceFormat::CoreProfile) {
        return ""
               "#version 150 core                                   \n"
               "uniform sampler2D source;                           \n"
               "uniform float qt_Opacity;                           \n"
               "in vec2 qt_TexCoord;                                \n"
               "out vec4 fragColor;                                 \n"
               "void main() {                                       \n"
               "   vec4 p = texture(source, qt_TexCoord);           \n"
               "   float a = qt_Opacity * p.a;                      \n"
               "   fragColor = vec4(p.rgb * a, a);                  \n"
               "}";
    } else {
        return ""
               "uniform highp sampler2D source;                         \n"
               "uniform highp float qt_Opacity;                         \n"
               "varying highp vec2 qt_TexCoord;                         \n"
               "void main() {                                           \n"
               "   highp vec4 p = texture2D(source, qt_TexCoord);       \n"
               "   highp float a = qt_Opacity * p.a;                    \n"
               "   gl_FragColor = vec4(p.rgb * a, a);                   \n"
               "}";
    }
}

void Q3DSLayer3DSGMaterialShader::initialize()
{
    m_matrixId = program()->uniformLocation("qt_Matrix");
    m_opacityId = program()->uniformLocation("qt_Opacity");
}

static inline bool isPowerOfTwo(int x)
{
    // Assumption: x >= 1
    return x == (x & -x);
}

void Q3DSLayer3DSGMaterialShader::updateState(const RenderState &state, QSGMaterial *newEffect, QSGMaterial *oldEffect)
{
    Q_ASSERT(!oldEffect || newEffect->type() == oldEffect->type());
    Q3DSLayer3DSGMaterial *tx = static_cast<Q3DSLayer3DSGMaterial *>(newEffect);
    Q3DSLayer3DSGMaterial *oldTx = static_cast<Q3DSLayer3DSGMaterial *>(oldEffect);

    QSGTexture *t = tx->texture();
    if (t) {
        QOpenGLContext *ctx = const_cast<QOpenGLContext *>(state.context());
        if (!ctx->functions()->hasOpenGLFeature(QOpenGLFunctions::NPOTTextureRepeat)) {
            const QSize size = t->textureSize();
            const bool isNpot = !isPowerOfTwo(size.width()) || !isPowerOfTwo(size.height());
            if (isNpot) {
                t->setHorizontalWrapMode(QSGTexture::ClampToEdge);
                t->setVerticalWrapMode(QSGTexture::ClampToEdge);
            }
        }
        if (!oldTx || oldTx->texture()->textureId() != t->textureId())
            t->bind();
        else
            t->updateBindOptions();
    }

    if (state.isMatrixDirty())
        program()->setUniformValue(m_matrixId, state.combinedMatrix());

    if (state.isOpacityDirty())
        program()->setUniformValue(m_opacityId, state.opacity());
}

void Q3DSLayer3DSGMaterial::setTexture(QSGTexture *texture)
{
    m_texture = texture;
    setFlag(Blending, m_texture ? m_texture->hasAlphaChannel() : false);
}

QSGMaterialType *Q3DSLayer3DSGMaterial::type() const
{
    return &materialType;
}

QSGMaterialShader *Q3DSLayer3DSGMaterial::createShader() const
{
    return new Q3DSLayer3DSGMaterialShader;
}

const char * const *Q3DSLayer3DSGMsaa2MaterialShader::attributeNames() const
{
    return q3ds_sg_vs_attributes();
}

const char *Q3DSLayer3DSGMsaa2MaterialShader::vertexShader() const
{
    return q3ds_sg_vs();
}

const char *Q3DSLayer3DSGMsaa2MaterialShader::fragmentShader() const
{
    // Make the result have pre-multiplied alpha.

    // Here we have 150 (3.2) core and ES 3.1 shaders only. Multisample
    // textures do not exist in ES < 3.1. This material is never set as the
    // active one with GLES < 3.1 (since the scenemanager takes care of
    // reverting sample count when setting up the scene), but handle all cases
    // gracefully regardless.

    const char prefix150[] =
            "#version 150 core                                                 \n";

    const char prefix310[] =
            "#version 310 es                                                   \n"
            "precision highp float;                                            \n";

    const char body[] =
            "uniform sampler2DMS source;                                       \n"
            "uniform float qt_Opacity;                                         \n"
            "in vec2 qt_TexCoord;                                              \n"
            "out vec4 fragColor;                                               \n"
            "void main() {                                                     \n"
            "   ivec2 tc = ivec2(floor(textureSize(source) * qt_TexCoord));    \n"
            "   vec4 p = texelFetch(source, tc, 0) + texelFetch(source, tc, 1);\n"
            "   p /= 2.0;                                                      \n"
            "   float a = qt_Opacity * p.a;                                    \n"
            "   fragColor = vec4(p.rgb * a, a);                                \n"
            "}";

    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    if (ctx->format().version() >= qMakePair(3, 2) && ctx->format().profile() == QSurfaceFormat::CoreProfile) {
        static char buf[sizeof(prefix150) + sizeof(body)] = "";
        if (buf[0] == '\0') {
            strcpy(buf, prefix150);
            strcat(buf, body);
        }
        return buf;
    } else if (ctx->isOpenGLES() && ctx->format().version() >= qMakePair(3, 1)) {
        static char buf[sizeof(prefix310) + sizeof(body)] = "";
        if (buf[0] == '\0') {
            strcpy(buf, prefix310);
            strcat(buf, body);
        }
        return buf;
    } else {
        return "void main() { gl_FragColor = vec4(0.0); }\n";
    }
}

const char *Q3DSLayer3DSGMsaa4MaterialShader::fragmentShader() const
{
    // almost like Msaa2 but takes 4 samples

    const char prefix150[] =
            "#version 150 core                                                 \n";

    const char prefix310[] =
            "#version 310 es                                                   \n"
            "precision highp float;                                            \n";

    const char body[] =
            "uniform sampler2DMS source;                                       \n"
            "uniform float qt_Opacity;                                         \n"
            "in vec2 qt_TexCoord;                                              \n"
            "out vec4 fragColor;                                               \n"
            "void main() {                                                     \n"
            "   ivec2 tc = ivec2(floor(textureSize(source) * qt_TexCoord));    \n"
            "   vec4 p = texelFetch(source, tc, 0) + texelFetch(source, tc, 1) \n"
            "     + texelFetch(source, tc, 2) + texelFetch(source, tc, 3);     \n"
            "   p /= 4.0;                                                      \n"
            "   float a = qt_Opacity * p.a;                                    \n"
            "   fragColor = vec4(p.rgb * a, a);                                \n"
            "}";

    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    if (ctx->format().version() >= qMakePair(3, 2) && ctx->format().profile() == QSurfaceFormat::CoreProfile) {
        static char buf[sizeof(prefix150) + sizeof(body)] = "";
        if (buf[0] == '\0') {
            strcpy(buf, prefix150);
            strcat(buf, body);
        }
        return buf;
    } else if (ctx->isOpenGLES() && ctx->format().version() >= qMakePair(3, 1)) {
        static char buf[sizeof(prefix310) + sizeof(body)] = "";
        if (buf[0] == '\0') {
            strcpy(buf, prefix310);
            strcat(buf, body);
        }
        return buf;
    } else {
        return "void main() { gl_FragColor = vec4(0.0); }\n";
    }
}

void Q3DSLayer3DSGMsaa2MaterialShader::initialize()
{
    m_matrixId = program()->uniformLocation("qt_Matrix");
    m_opacityId = program()->uniformLocation("qt_Opacity");
}

void Q3DSLayer3DSGMsaa2MaterialShader::updateState(const RenderState &state, QSGMaterial *newEffect, QSGMaterial *oldEffect)
{
    Q_ASSERT(!oldEffect || newEffect->type() == oldEffect->type());
    Q3DSLayer3DSGMaterial *tx = static_cast<Q3DSLayer3DSGMaterial *>(newEffect);
    Q3DSLayer3DSGMaterial *oldTx = static_cast<Q3DSLayer3DSGMaterial *>(oldEffect);

    QSGTexture *t = tx->texture();
    if (t) {
        QOpenGLContext *ctx = QOpenGLContext::currentContext();
        QOpenGLFunctions *f = ctx->functions();

        // This below does only the bare minimum, ignoring most of the
        // QSGTexture settings since we know what Studio3D and Studio3DEngine
        // wants. For anything more we should rather have Qt Quick (QSGTexture)
        // upgraded with support for multisample textures.

        f->glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, t->textureId());
        if (!oldTx || oldTx->texture()->textureId() != t->textureId()) {
            f->glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            f->glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            f->glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            f->glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        }
    }

    if (state.isMatrixDirty())
        program()->setUniformValue(m_matrixId, state.combinedMatrix());

    if (state.isOpacityDirty())
        program()->setUniformValue(m_opacityId, state.opacity());
}

void Q3DSLayer3DSGMsaa2Material::setTexture(QSGTexture *texture)
{
    m_texture = texture;
    setFlag(Blending, m_texture ? m_texture->hasAlphaChannel() : false);
}

QSGMaterialType *Q3DSLayer3DSGMsaa2Material::type() const
{
    return &msaa2MaterialType;
}

QSGMaterialShader *Q3DSLayer3DSGMsaa2Material::createShader() const
{
    return new Q3DSLayer3DSGMsaa2MaterialShader;
}

QSGMaterialType *Q3DSLayer3DSGMsaa4Material::type() const
{
    return &msaa4MaterialType;
}

QSGMaterialShader *Q3DSLayer3DSGMsaa4Material::createShader() const
{
    return new Q3DSLayer3DSGMsaa4MaterialShader;
}

Q3DSLayer3DSGNode::Q3DSLayer3DSGNode()
    : m_geometry(QSGGeometry::defaultAttributes_TexturedPoint2D(), 4)
{
    setMaterial(&m_material);
    setGeometry(&m_geometry);
}

Q3DSLayer3DSGNode::~Q3DSLayer3DSGNode()
{
    if (m_ownsTexture)
        delete m_material.texture();
}

QSGTexture *Q3DSLayer3DSGNode::texture() const
{
    return m_material.texture();
}

int Q3DSLayer3DSGNode::sampleCount() const
{
    return m_sampleCount;
}

void Q3DSLayer3DSGNode::setTexture(QSGTexture *texture, int msaaSampleCount)
{
    if (m_material.texture() == texture)
        return;

    if (m_ownsTexture)
        delete m_material.texture();

    m_material.setTexture(texture);
    m_msaa2Material.setTexture(texture);
    m_msaa4Material.setTexture(texture);

    QSGMaterial *newMaterial = &m_material;
    if (msaaSampleCount > 1) {
        switch (msaaSampleCount) {
        case 2:
            newMaterial = &m_msaa2Material;
            break;
        case 4:
            newMaterial = &m_msaa4Material;
            break;
        default:
            break;
        }
    }
    if (newMaterial != material())
        setMaterial(newMaterial);

    m_sampleCount = msaaSampleCount;

    markDirty(DirtyMaterial);
}

QRectF Q3DSLayer3DSGNode::rect() const
{
    return m_rect;
}

void Q3DSLayer3DSGNode::setRect(const QRectF &rect)
{
    if (rect == m_rect)
        return;

    m_rect = rect;

    const QRectF sourceRect(0.0f, 1.0f, 1.0f, -1.0f); // MirrorVertically
    QSGGeometry::updateTexturedRectGeometry(&m_geometry, m_rect, sourceRect);

    markDirty(DirtyGeometry);
}

bool Q3DSLayer3DSGNode::ownsTexture() const
{
    return m_ownsTexture;
}

void Q3DSLayer3DSGNode::setOwnsTexture(bool owns)
{
    m_ownsTexture = owns;
}

QT_END_NAMESPACE
