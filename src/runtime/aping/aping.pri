SOURCES += \
    $$PWD/q3dsstudio3dengine.cpp \
    $$PWD/q3dslayer3dsgnode.cpp \
    $$PWD/q3dslayer3d.cpp \
    $$PWD/q3dsobject3d.cpp \
    $$PWD/q3dsnode3d.cpp \
    $$PWD/q3dsgroup3d.cpp \
    $$PWD/q3dsmodel3d.cpp \
    $$PWD/q3dscamera3d.cpp \
    $$PWD/q3dslight3d.cpp

HEADERS += \
    $$PWD/q3dsstudio3dengine_p.h \
    $$PWD/q3dslayer3dsgnode_p.h \
    $$PWD/q3dslayer3d_p.h \
    $$PWD/q3dsobject3d_p.h \
    $$PWD/q3dsnode3d_p.h \
    $$PWD/q3dsgroup3d_p.h \
    $$PWD/q3dsmodel3d_p.h \
    $$PWD/q3dscamera3d_p.h \
    $$PWD/q3dslight3d_p.h
