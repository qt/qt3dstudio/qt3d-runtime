/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSLAYER3DSGNODE_P_H
#define Q3DSLAYER3DSGNODE_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DStudioRuntime2/private/q3dsruntimeglobal_p.h>
#include <QSGGeometryNode>
#include <QSGMaterial>
#include <QSGMaterialShader>
#include <QSGTexture>

QT_BEGIN_NAMESPACE

class Q3DSLayer3DSGMaterialShader : public QSGMaterialShader
{
public:
    void updateState(const RenderState &state, QSGMaterial *newMaterial, QSGMaterial *oldMaterial) override;
    const char * const *attributeNames() const override;

protected:
    void initialize() override;
    const char *vertexShader() const override;
    const char *fragmentShader() const override;

private:
    int m_matrixId = -1;
    int m_opacityId = -1;
};

class Q3DSLayer3DSGMaterial : public QSGMaterial
{
public:
    void setTexture(QSGTexture *texture);
    QSGTexture *texture() const { return m_texture; }

    QSGMaterialType *type() const override;
    QSGMaterialShader *createShader() const override;

private:
    QSGTexture *m_texture = nullptr;
};

class Q3DSLayer3DSGMsaa2MaterialShader : public QSGMaterialShader
{
public:
    void updateState(const RenderState &state, QSGMaterial *newMaterial, QSGMaterial *oldMaterial) override;
    const char * const *attributeNames() const override;

protected:
    void initialize() override;
    const char *vertexShader() const override;
    const char *fragmentShader() const override;

private:
    int m_matrixId = -1;
    int m_opacityId = -1;
};

class Q3DSLayer3DSGMsaa2Material : public QSGMaterial
{
public:
    void setTexture(QSGTexture *texture);
    QSGTexture *texture() const { return m_texture; }

    QSGMaterialType *type() const override;
    QSGMaterialShader *createShader() const override;

private:
    QSGTexture *m_texture = nullptr;
};

class Q3DSLayer3DSGMsaa4MaterialShader : public Q3DSLayer3DSGMsaa2MaterialShader
{
protected:
    const char *fragmentShader() const override;
};

class Q3DSLayer3DSGMsaa4Material : public Q3DSLayer3DSGMsaa2Material
{
public:
    QSGMaterialType *type() const override;
    QSGMaterialShader *createShader() const override;
};

class Q3DSV_PRIVATE_EXPORT Q3DSLayer3DSGNode : public QSGGeometryNode
{
public:
    Q3DSLayer3DSGNode();
    ~Q3DSLayer3DSGNode();

    QSGTexture *texture() const;
    int sampleCount() const;
    void setTexture(QSGTexture *texture, int msaaSampleCount = 0);

    QRectF rect() const;
    void setRect(const QRectF &rect);

    bool ownsTexture() const;
    void setOwnsTexture(bool owns);

private:
    QSGGeometry m_geometry;
    QRectF m_rect;
    Q3DSLayer3DSGMaterial m_material;
    Q3DSLayer3DSGMsaa2Material m_msaa2Material;
    Q3DSLayer3DSGMsaa4Material m_msaa4Material;
    bool m_ownsTexture = false;
    int m_sampleCount = 0;
};

QT_END_NAMESPACE

#endif // Q3DSLAYER3DSGNODE_P_H
