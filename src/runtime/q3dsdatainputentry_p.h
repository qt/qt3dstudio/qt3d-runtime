/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSDATAINPUTENTRY_P_H
#define Q3DSDATAINPUTENTRY_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "q3dsruntimeglobal_p.h"
#include <QVector>
#include <QString>
#include <QHash>
#include <QVariant>
#include <QDataStream>
#include <QByteArray>

QT_BEGIN_NAMESPACE

struct Q3DSV_PRIVATE_EXPORT Q3DSDataInputEntry
{
    typedef QHash<QString, Q3DSDataInputEntry> Map;
    // Metadata key - Datainput name mapping for faster lookups
    // when enquiring datainputs that have metadata with specified key.
    typedef QMultiHash<QVariant, QString> MetadataMap;

    enum Type {
        TypeString,
        TypeFloat,
        TypeRangedNumber,
        TypeVec2,
        TypeVec3,
        TypeVec4,
        TypeVariant,
        TypeBoolean
    };

    QString name;
    Type type = TypeString;
    float minValue = 0;
    float maxValue = 0;

    QHash<QVariant, QVariant> metadata;

    // Just check that relative minmax values are sane, which also
    // covers the case when both are at default value of 0.0 i.e. unset.
    bool hasMinMax() const { return maxValue > minValue; }
};

inline uint qHash(const QVariant &key)
{
    QByteArray ar;
    QDataStream ds(&ar, QIODevice::WriteOnly);

    ds << key;

    return qHashBits(ar, ds.device()->pos());
}

Q_DECLARE_TYPEINFO(Q3DSDataInputEntry, Q_MOVABLE_TYPE);

QT_END_NAMESPACE

#endif // Q3DSDATAINPUTENTRY_P_H
