/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_OPENGLVERTEXARRAYOBJECT_H
#define QT3DRENDER_DRAGON_OPENGLVERTEXARRAYOBJECT_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DCore/QNodeId>

#include <QtGui/qopenglvertexarrayobject.h>

#include <private/dragonglbuffer_p.h>
#include <private/dragonmutable_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

class GeometryManager;
class ShaderManager;

// geometry id + shader id
using VAOIdentifier = QPair<Qt3DCore::QNodeId, Qt3DCore::QNodeId>;

class GLVertexArrayObject
{
public:
    GLVertexArrayObject();

    void bind();
    void release();

//    void create(SubmissionContext *ctx, const VAOIdentifier &key);
    void destroy();

    bool isAbandoned(GeometryManager *geomMgr, ShaderManager *shaderMgr);

    QOpenGLVertexArrayObject *vao() { return m_vao.data(); }
    const QOpenGLVertexArrayObject *vao() const { return m_vao.data(); }

    void setSpecified(bool b) { m_specified = b; }
    bool isSpecified() const { return m_specified; }


//private:
//    QMutex m_mutex;
//    SubmissionContext *m_ctx;
    QSharedPointer<QOpenGLVertexArrayObject> m_vao;
    bool m_specified;
    bool m_supportsVao;
    VAOIdentifier m_owners;

    friend class SubmissionContext;

    struct VAOVertexAttribute
    {
        // TODO should we just store the Attribute instead of copying this?
        GLBuffer::Type attributeType;
        int location;
        GLint dataType;
        uint byteOffset;
        uint vertexSize;
        uint byteStride;
        uint divisor;
        GLenum shaderDataType;
    };

    void saveVertexAttribute(const VAOVertexAttribute &attr);

    QVector<VAOVertexAttribute> m_vertexAttributes;
    Mutable<GLBuffer> m_indexAttribute;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_OPENGLVERTEXARRAYOBJECT_H
