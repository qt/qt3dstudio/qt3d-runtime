HEADERS += \
    $$PWD/dragonattribute_p.h \
    $$PWD/dragonbuffer_p.h \
    $$PWD/dragongeometry_p.h \
    $$PWD/dragonsphere_p.h \
    $$PWD/dragonbuffervisitor_p.h \
    $$PWD/dragonbufferutils_p.h \
    $$PWD/dragongeometryrenderer_p.h

SOURCES += \
    $$PWD/dragonattribute.cpp \
    $$PWD/dragonbuffer.cpp \
    $$PWD/dragongeometry.cpp \
    $$PWD/dragonsphere.cpp \
    $$PWD/dragongeometryrenderer.cpp
