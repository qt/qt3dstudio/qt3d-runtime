/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_OPTIONAL_P_H
#define QT3DRENDER_DRAGON_OPTIONAL_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DRender/qt3drender_global.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

// TODO replace with std::optional when we support C++17

/*! \internal
 * Helper class to store values that have an unknown default values (such as clearColor)
 */
template<typename T>
class Optional
{
public:
    Optional() = default;

    Optional(T t)
        : m_value(t)
        , m_hasValue(true)
    {
    }

    T *operator->() { return m_value.operator->(); }
    const T *operator->() const { return m_value.operator->(); }

    T &get() { return m_value; }
    const T &get() const { return m_value; }

    bool operator==(const T &other) const { return m_value == other; }

    bool operator!=(const T &other) const { return m_value != other; }

    bool hasValue() const
    {
        return m_hasValue;
    }

    bool has_value() const
    {
        return m_hasValue;
    }

private:
    T m_value = T();
    bool m_hasValue = false;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_OPTIONAL_P_H
