HEADERS += \
    $$PWD/dragoneffect_p.h \
    $$PWD/dragonfilterkey_p.h \
    $$PWD/dragonmaterial_p.h \
    $$PWD/dragonparameter_p.h \
    $$PWD/dragonrenderpass_p.h \
    $$PWD/dragonshader_p.h \
    $$PWD/dragonshaderdata_p.h \
    $$PWD/dragontechnique_p.h

SOURCES += \
    $$PWD/dragoneffect.cpp \
    $$PWD/dragonfilterkey.cpp \
    $$PWD/dragonmaterial.cpp \
    $$PWD/dragonparameter.cpp \
    $$PWD/dragonrenderpass.cpp \
    $$PWD/dragonshader.cpp \
    $$PWD/dragonshaderdata.cpp \
    $$PWD/dragontechnique.cpp
