INCLUDEPATH += $$PWD

include(materialsystem/materialsystem.pri)
include(geometry/geometry.pri)
include(framegraph/framegraph.pri)
include(jobs/jobs.pri)
include(renderer/renderer.pri)
include(renderstates/renderstates.pri)
include(graphicshelpers/graphicshelpers.pri)

QT_PRIVATE += openglextensions

#*-msvc*:QMAKE_CXXFLAGS += /showIncludes

HEADERS += \
    $$PWD/dragonattachment_p.h \
    $$PWD/dragonbackendnode_p.h \
    $$PWD/dragontexture_p.h \
    $$PWD/dragontrackingchangescontainer_p.h \
    $$PWD/dragontextureimage_p.h \
    $$PWD/dragonrenderview_p.h \
    $$PWD/dragonentity_p.h \
    $$PWD/dragontransform_p.h \
    $$PWD/dragonrendersettings_p.h \
    $$PWD/dragoncameralens_p.h \
    $$PWD/dragonmapper_p.h \
    $$PWD/dragonvaluecontainer_p.h \
    $$PWD/dragonrenderer_p.h \
    $$PWD/dragonrendertarget_p.h \
    $$PWD/dragonrendertargetoutput_p.h \
    $$PWD/dragonglshader_p.h \
    $$PWD/dragonglbuffer_p.h \
    $$PWD/dragonrendercommand_p.h \
    $$PWD/dragongltexture_p.h \
    $$PWD/dragongltypehelpers_p.h \
    $$PWD/dragonoffscreensurfacehelper_p.h \
    $$PWD/dragontask_p.h \
    $$PWD/qdragonrenderaspect_p.h \
    $$PWD/dragonparameterpack_p.h \
    $$PWD/dragongraphicsutils_p.h \
    $$PWD/dragonrenderbuffer_p.h \
    $$PWD/dragonactivatedsurface_p.h \
    $$PWD/dragonoptional_p.h \
    $$PWD/dragonstringtoint_p.h \
    $$PWD/dragonimmutable_p.h \
    $$PWD/dragonmutable_p.h \
    $$PWD/dragoncomparegenerators_p.h \
    $$PWD/dragontextureproperties_p.h \
    $$PWD/dragonscene2d_p.h \
    $$PWD/dragonnodetree_p.h

SOURCES += \
    $$PWD/dragonattachment.cpp \
    $$PWD/dragonbackendnode.cpp \
    $$PWD/dragontexture.cpp \
    $$PWD/dragontrackingchangescontainer.cpp \
    $$PWD/dragontextureimage.cpp \
    $$PWD/dragonrenderview.cpp \
    $$PWD/dragonentity.cpp \
    $$PWD/dragontransform.cpp \
    $$PWD/dragonrendersettings.cpp \
    $$PWD/dragoncameralens.cpp \
    $$PWD/dragonmapper.cpp \
    $$PWD/dragonrenderer.cpp \
    $$PWD/dragonrendertarget.cpp \
    $$PWD/dragonrendertargetoutput.cpp \
    $$PWD/dragonglshader.cpp \
    $$PWD/dragonglbuffer.cpp \
    $$PWD/dragonrendercommand.cpp \
    $$PWD/dragongltexture.cpp \
    $$PWD/dragongltypehelpers.cpp \
    $$PWD/dragonoffscreensurfacehelper.cpp \
    $$PWD/qdragonrenderaspect.cpp \
    $$PWD/dragonparameterpack.cpp \
    $$PWD/dragonrenderbuffer.cpp \
    $$PWD/dragonactivatedsurface.cpp \
    $$PWD/dragonoptional.cpp \
    $$PWD/dragonstringtoint.cpp \
    $$PWD/dragonscene2d.cpp
