/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonrendersettings_p.h"

#include <Qt3DRender/QFrameGraphNode>
#include <Qt3DRender/private/abstractrenderer_p.h>
#include <Qt3DRender/private/qrendersettings_p.h>
#include <Qt3DCore/qpropertyupdatedchange.h>

#include <private/qdragonrenderaspect_p.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

RenderSettings::RenderSettings(NodeTree *nodeTree)
    : Qt3DCore::QBackendNode()
    , m_renderPolicy(QRenderSettings::OnDemand)
    , m_pickMethod(QPickingSettings::BoundingVolumePicking)
    , m_pickResultMode(QPickingSettings::NearestPick)
    , m_faceOrientationPickingMode(QPickingSettings::FrontFace)
    , m_pickWorldSpaceTolerance(.1f)
    , m_activeFrameGraph()
    , m_nodeTree(nodeTree)
{
}

// TODO see if maybe all that was needed was for this to have a parent?

void RenderSettings::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    m_nodeTree->addNode(peerId());
    m_nodeTree->setParent(peerId(), change->parentId());

    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QRenderSettingsData>>(change);
    const auto &data = typedChange->data;
    m_activeFrameGraph = data.activeFrameGraphId;
    m_renderPolicy = data.renderPolicy;
    m_pickMethod = data.pickMethod;
    m_pickResultMode = data.pickResultMode;
    m_pickWorldSpaceTolerance = data.pickWorldSpaceTolerance;
    m_faceOrientationPickingMode = data.faceOrientationPickingMode;
}

void RenderSettings::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    // TODO make sure these changes are properly notified
    if (e->type() == PropertyUpdated) {
        QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<QPropertyUpdatedChange>(e);
        if (propertyChange->propertyName() == QByteArrayLiteral("pickMethod"))
            m_pickMethod = propertyChange->value().value<QPickingSettings::PickMethod>();
        else if (propertyChange->propertyName() == QByteArrayLiteral("pickResult"))
            m_pickResultMode = propertyChange->value().value<QPickingSettings::PickResultMode>();
        else if (propertyChange->propertyName() == QByteArrayLiteral("faceOrientationPickingMode"))
            m_faceOrientationPickingMode = propertyChange->value().value<QPickingSettings::FaceOrientationPickingMode>();
        else if (propertyChange->propertyName() == QByteArrayLiteral("pickWorldSpaceTolerance"))
            m_pickWorldSpaceTolerance = propertyChange->value().toFloat();
        else if (propertyChange->propertyName() == QByteArrayLiteral("activeFrameGraph"))
            m_activeFrameGraph = propertyChange->value().value<QNodeId>();
        else if (propertyChange->propertyName() == QByteArrayLiteral("renderPolicy"))
            m_renderPolicy = propertyChange->value().value<QRenderSettings::RenderPolicy>();
        else if (propertyChange->propertyName() == QByteArrayLiteral("parent")) {
            m_nodeTree->setParent(peerId(), propertyChange->value().value<QNodeId>());
        }
//        markDirty(AbstractRenderer::AllDirty);
    }

    QBackendNode::sceneChangeEvent(e);
}

RenderSettingsFunctor::RenderSettingsFunctor(QDragonRenderAspect *renderer, NodeTree *nodeTree)
    : m_renderer(renderer)
    , m_nodeTree(nodeTree)
{
}

Qt3DCore::QBackendNode *RenderSettingsFunctor::create(const Qt3DCore::QNodeCreatedChangeBasePtr &change) const
{
    Q_UNUSED(change);
    if (m_renderer->m_renderSettings != nullptr) {
        qWarning() << "Renderer settings already exists";
        return nullptr;
    }

    RenderSettings *settings = new RenderSettings(m_nodeTree);
    m_renderer->m_renderSettings = settings;
    return settings;
}

Qt3DCore::QBackendNode *RenderSettingsFunctor::get(Qt3DCore::QNodeId id) const
{
    Q_UNUSED(id);
    return m_renderer->m_renderSettings;
}

void RenderSettingsFunctor::destroy(Qt3DCore::QNodeId id) const
{
    Q_UNUSED(id);
    // Deletes the old settings object
    auto settings = m_renderer->m_renderSettings;
    delete settings;
    m_renderer->m_renderSettings = nullptr;
}

} // namespace Render
} // namespace Qt3DRender

QT_END_NAMESPACE
