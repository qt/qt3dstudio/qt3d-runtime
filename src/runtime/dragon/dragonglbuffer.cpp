/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonglbuffer_p.h"

#include <private/dragonactivatedsurface_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

//GLBuffer::GLBuffer(const GLBuffer &other)
//{
//    Q_UNUSED(other);
//    Q_ASSERT_X(false,
//               Q_FUNC_INFO,
//               "GLBuffer: should never be copied. Make sure you're not hanging on to a copy somewhere that forces us to do a copy-on-write.");
//}

GLBuffer::GLBuffer(GLBuffer &&other)
    : m_bufferId(other.m_bufferId)
    , m_glContext(other.m_glContext)
{
    other.m_bufferId = 0;
    other.m_glContext = nullptr;
}

GLBuffer::GLBuffer(const ActivatedSurface &surface, const Immutable<LoadedBuffer> &buffer)
    : m_glContext(surface.openGLContext())
{
    GLuint bufferId = 0;
    QOpenGLFunctions *f = m_glContext->functions();
    f->glGenBuffers(1, &bufferId);
    // Since we are uploading, the type doesn't matter
    GLenum glType = GL_ARRAY_BUFFER;
    f->glBindBuffer(glType, bufferId);
    // TODO: Why do we first orphan the buffer?
    f->glBufferData(glType, buffer->data.size(), nullptr, GL_STATIC_DRAW);
    f->glBufferData(glType,
                    buffer->data.size(),
                    buffer->data.constData(),
                    GL_STATIC_DRAW);
    m_bufferId = bufferId;
}

GLBuffer &GLBuffer::operator=(GLBuffer &&other)
{
    m_bufferId = other.m_bufferId;
    m_glContext = other.m_glContext;

    other.m_bufferId = 0;
    other.m_glContext = nullptr;
    return *this;
}

GLBuffer::~GLBuffer()
{
    if (m_bufferId != 0) {
        Q_ASSERT(m_glContext != nullptr);
        Q_ASSERT(m_glContext == QOpenGLContext::currentContext());
        m_glContext->functions()->glDeleteBuffers(1, &m_bufferId);
        m_bufferId = 0;
    }
}

GLuint GLBuffer::bufferId() const
{
    return m_bufferId;
}

void GLBuffer::reload(const Immutable<LoadedBuffer> &loadedBuffer)
{
    GLenum glType = GL_ARRAY_BUFFER;
    QOpenGLFunctions *f = m_glContext->functions();
    f->glBindBuffer(glType, m_bufferId);
    f->glBufferData(glType, loadedBuffer->data.size(),
                    nullptr,
                    GL_STATIC_DRAW);
    f->glBufferData(glType, loadedBuffer->data.size(),
                    loadedBuffer->data.constData(),
                    GL_STATIC_DRAW);
}

} // namespace Dragon
} // namespace Qt3DRender
QT_END_NAMESPACE
