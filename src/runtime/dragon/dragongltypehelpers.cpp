/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragongltypehelpers_p.h"

#include <private/dragonglbuffer_p.h>
#include <Qt3DRender/qattribute.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {
namespace GLTypes {

GLint glDataTypeFromAttributeDataType(QAttribute::VertexBaseType dataType)
{
    switch (dataType) {
    case QAttribute::Byte:
        return GL_BYTE;
    case QAttribute::UnsignedByte:
        return GL_UNSIGNED_BYTE;
    case QAttribute::Short:
        return GL_SHORT;
    case QAttribute::UnsignedShort:
        return GL_UNSIGNED_SHORT;
    case QAttribute::Int:
        return GL_INT;
    case QAttribute::UnsignedInt:
        return GL_UNSIGNED_INT;
    case QAttribute::HalfFloat:
#ifdef GL_HALF_FLOAT
        return GL_HALF_FLOAT;
#endif
#ifndef QT_OPENGL_ES_2 // Otherwise compile error as Qt defines GL_DOUBLE as GL_FLOAT when using ES2
    case QAttribute::Double:
        return GL_DOUBLE;
#endif
    case QAttribute::Float:
        break;
    default:
        qWarning() << Q_FUNC_INFO << "unsupported dataType:" << dataType;
    }
    return GL_FLOAT;
}

GLBuffer::Type attributeTypeToGLBufferType(QAttribute::AttributeType type)
{
    switch (type) {
    case QAttribute::VertexAttribute:
        return GLBuffer::ArrayBuffer;
    case QAttribute::IndexAttribute:
        return GLBuffer::IndexBuffer;
    case QAttribute::DrawIndirectAttribute:
        return GLBuffer::DrawIndirectBuffer;
    default:
        Q_UNREACHABLE();
    }
}

GLuint byteSizeFromType(GLint type)
{
    switch (type) {
    case GL_FLOAT:
        return sizeof(float);
#ifndef QT_OPENGL_ES_2 // Otherwise compile error as Qt defines GL_DOUBLE as GL_FLOAT when using ES2
    case GL_DOUBLE:
        return sizeof(double);
#endif
    case GL_UNSIGNED_BYTE:
        return sizeof(unsigned char);
    case GL_UNSIGNED_INT:
        return sizeof(GLuint);

    case GL_FLOAT_VEC2:
        return sizeof(float) * 2;
    case GL_FLOAT_VEC3:
        return sizeof(float) * 3;
    case GL_FLOAT_VEC4:
        return sizeof(float) * 4;
#ifdef GL_DOUBLE_VEC3 // Required to compile on pre GL 4.1 systems
    case GL_DOUBLE_VEC2:
        return sizeof(double) * 2;
    case GL_DOUBLE_VEC3:
        return sizeof(double) * 3;
    case GL_DOUBLE_VEC4:
        return sizeof(double) * 4;
#endif
    default:
        qWarning() << Q_FUNC_INFO << "unsupported:" << QString::number(type, 16);
    }

    return 0;
}

} // namespace GLTypes
} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
