HEADERS += \
    $$PWD/dragonjobs_common_p.h \
    $$PWD/dragonbufferjobs_p.h \
    $$PWD/dragonmaterialjobs_p.h \
    $$PWD/dragonrenderviewjobs_p.h \
    $$PWD/dragontexturejobs_p.h \
    $$PWD/dragontransformjobs_p.h \
    $$PWD/dragontreejobs_p.h \
    $$PWD/dragonboundingvolumejobs_p.h

SOURCES += \
    $$PWD/dragonjobs_common.cpp \
    $$PWD/dragonbufferjobs.cpp \
    $$PWD/dragonmaterialjobs.cpp \
    $$PWD/dragonrenderviewjobs.cpp \
    $$PWD/dragontexturejobs.cpp \
    $$PWD/dragontransformjobs.cpp \
    $$PWD/dragontreejobs.cpp \
    $$PWD/dragonboundingvolumejobs.cpp
