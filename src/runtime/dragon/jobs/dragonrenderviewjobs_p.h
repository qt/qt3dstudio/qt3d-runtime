/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_RENDERVIEWJOBS_H
#define QT3DRENDER_DRAGON_RENDERVIEWJOBS_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonentity_p.h>
#include <private/dragonframegraphnode_p.h>
#include <private/dragonrenderview_p.h>
#include <private/dragonvaluecontainer_p.h>
#include <private/dragonmaterialjobs_p.h>
#include <private/dragontreejobs_p.h>

#include <private/dragonrendercommand_p.h>

#include <Qt3DRender/private/qgraphicsapifilter_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

class RenderTarget;
class RenderTargetOutput;
class RenderPass;
class Parameter;
class ShaderData;
class Texture;
class RenderStateNode;
struct BoundingVolume;
struct LocalBoundingVolumeResult;

// Key is the id of the leaf node of the render view
// The same key is used by the render commands and parameter gatherer
using RenderViews = ValueContainer<RenderView>;

struct RenderPassParameterIds
{
    Immutable<RenderPass> renderPass;
    ParameterNameToIdMap parameterIds;
    ParameterNameToParameterMap parameters;
};

struct MaterialParameterIds
{
    QHash<Qt3DCore::QNodeId, RenderPassParameterIds> renderPassParameterIds;
};

struct ParameterTarget
{
    Qt3DCore::QNodeId renderViewId;
    Qt3DCore::QNodeId materialId;
    Qt3DCore::QNodeId renderPassId;
};

struct RenderViewParameterIds
{
    QHash<Qt3DCore::QNodeId, MaterialParameterIds> materialParameterIds;
    QHash<Qt3DCore::QNodeId, QVector<ParameterTarget>> parameterToRenderPass;
};

using GatheredParameters = ValueContainer<RenderViewParameterIds>;

struct RenderViewCommands
{
    Immutable<RenderView> renderView;
    QVector<Immutable<RenderCommand>> commands;
};

using RenderCommands = ValueContainer<RenderViewCommands>;

// TODO this is getting ugly, consider storing RenderCommand and AppliedRenderCommand in a ValueContainer
// each, with some kind of ID we can track, and have them point to their respective views
struct AppliedRenderCommand
{
    Immutable<RenderCommand> renderCommand;
    ParameterNameToParameterMap m_parameterMap;
    Immutable<Matrix4x4> m_worldTransform;
};

struct AppliedRenderCommands
{
    QVector<Immutable<AppliedRenderCommand>> commands;
};

using AppliedRenderCommandss = ValueContainer<AppliedRenderCommands>;
using WorldBoundingVolumes = ValueContainer<BoundingVolume>;

RenderViews buildRenderViews(RenderViews renderViews,
                             const TreeInfo &frameGraph,
                             const ValueContainer<FrameGraphNode> &frameGraphNodes,
                             const ValueContainer<Entity> &entities,
                             const ValueContainer<CameraLens> &cameraLenses,
                             const ValueContainer<RenderTarget> &renderTargets,
                             const ValueContainer<RenderTargetOutput> &renderTargetOutputs,
                             const ValueContainer<RenderStateNode> &renderStates);

// TODO add a struct that combines these to reduce the number of arguments
RenderCommands buildDrawRenderCommands(RenderCommands renderCommands,
                                       const ValueContainer<RenderView> &renderViews,
                                       const ValueContainer<Entity> &entities,
                                       const ValueContainer<Material> &materials,
                                       const ValueContainer<Geometry> &geometries,
                                       const ValueContainer<GeometryRenderer> &geometryRenderers,
                                       const ValueContainer<Shader> &shaders,
                                       const ValueContainer<RenderStateNode> &renderStates,
                                       const GatheredParameters &gatheredParameters);

GatheredParameters gatherMaterialParameters(GatheredParameters gatherResult,
                                            const RenderViews &renderViews,
                                            const GraphicsApiFilterData &contextInfo,
                                            const ValueContainer<Parameter> &parameters,
                                            const ValueContainer<Material> &materials,
                                            const ValueContainer<Effect> &effects,
                                            const ValueContainer<Technique> &techniques,
                                            const ValueContainer<RenderPass> &renderPasses,
                                            const ValueContainer<FilterKey> &filterKeys);

RenderCommands sortRenderCommands(RenderCommands renderCommands, RenderCommands renderCommandsIn,
                                  const WorldBoundingVolumes &worldBoundingVolumes,
                                  const ValueContainer<CameraMatrices> &renderViewCameraMatrices);

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_RENDERVIEWJOBS_H
