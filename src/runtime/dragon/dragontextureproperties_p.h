/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_TEXTUREPROPERTIES_H
#define QT3DRENDER_DRAGON_TEXTUREPROPERTIES_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DRender/qabstracttexture.h>
#include <Qt3DRender/qtexturewrapmode.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

/**
 * General, constant properties of a texture
 */
struct TextureProperties
{
    int width = 1;
    int height = 1;
    int depth = 1;
    int layers = 1;
    int mipLevels = 1;
    int samples = 1;
    QAbstractTexture::Target target = QAbstractTexture::Target2D;
    QAbstractTexture::TextureFormat format = QAbstractTexture::RGBA8_UNorm;
    bool generateMipMaps = false;

    bool operator==(const TextureProperties &o) const
    {
        return (width == o.width) && (height == o.height) && (depth == o.depth)
            && (layers == o.layers) && (mipLevels == o.mipLevels) && (target == o.target)
            && (format == o.format) && (generateMipMaps == o.generateMipMaps)
            && (samples == o.samples);
    }
    inline bool operator!=(const TextureProperties &o) const { return !(*this == o); }
};

/**
 *   Texture parameters that are independent of texture data and that may
 *   change without the re-uploading the texture
 */
struct TextureParameters
{
    QAbstractTexture::Filter magnificationFilter = QAbstractTexture::Nearest;
    QAbstractTexture::Filter minificationFilter = QAbstractTexture::Nearest;
    QTextureWrapMode::WrapMode wrapModeX = QTextureWrapMode::ClampToEdge;
    QTextureWrapMode::WrapMode wrapModeY = QTextureWrapMode::ClampToEdge;
    QTextureWrapMode::WrapMode wrapModeZ = QTextureWrapMode::ClampToEdge;
    float maximumAnisotropy = 1.0f;
    QAbstractTexture::ComparisonFunction comparisonFunction = QAbstractTexture::CompareLessEqual;
    QAbstractTexture::ComparisonMode comparisonMode = QAbstractTexture::CompareNone;

    bool operator==(const TextureParameters &o) const
    {
        return (magnificationFilter == o.magnificationFilter)
            && (minificationFilter == o.minificationFilter) && (wrapModeX == o.wrapModeX)
            && (wrapModeY == o.wrapModeY) && (wrapModeZ == o.wrapModeZ)
            && qFuzzyCompare(maximumAnisotropy, o.maximumAnisotropy)
            && (comparisonFunction == o.comparisonFunction) && (comparisonMode == o.comparisonMode);
    }
    inline bool operator!=(const TextureParameters &o) const { return !(*this == o); }
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_TEXTUREPROPERTIES_H
