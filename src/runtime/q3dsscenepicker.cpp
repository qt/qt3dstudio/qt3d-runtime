/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsscenepicker_p.h"
#include "q3dsscenemanager_p.h"
#include "q3dsinputmanager_p.h"

Q3DSScenePicker::Q3DSScenePicker(Q3DSSceneManager *manager)
    : m_inputManager(manager->inputManager())
{
}

Q3DSScenePicker::~Q3DSScenePicker()
{

}

QPoint Q3DSScenePicker::pickPoint() const
{
    return m_point;
}

Q3DSLayerNode *Q3DSScenePicker::pickLayer() const
{
    return m_layer;
}

bool Q3DSScenePicker::isHit() const
{
    return m_pickResults.size() > 0;
}

void Q3DSScenePicker::pick(const QPoint &point, Q3DSLayerNode *targetLayer)
{
    m_point = point;
    m_layer = targetLayer;
    m_inputManager->pickScene(this);
}

void Q3DSScenePicker::addPick(Q3DSGraphObject *object, qreal distance)
{
    PickResult result;
    result.m_object = object;
    result.m_distance = distance;

    m_pickResults.push_back(result);
}

void Q3DSScenePicker::setState(Q3DSScenePicker::PickState state)
{
    m_pickState = state;
    if (m_pickState == Ready || m_pickState == Failed)
        Q_EMIT ready();
}

void Q3DSScenePicker::setLayerState(Q3DSLayerNode *layer, Q3DSScenePicker::PickState state)
{
    m_layerStates[layer] = state;

    // First state transitions to queued or triggered will change the entire pick state
    if (m_pickState == Unqueued && state == Queued)
        m_pickState = Queued;
    if (m_pickState == Queued && state == Triggered)
        m_pickState = Triggered;

    // If all layer states are set to either Failed or Ready, also change picker state accordingly
    // If all layers failed, then the entire pick failed
    const auto states = m_layerStates.values();
    bool hasReady = false;
    for (const auto state : states) {
        if (state != Failed && state != Ready)
            return;
        else if (state == Ready)
            hasReady = true;
    }

    setState(hasReady ? Ready : Failed);
}

void Q3DSScenePicker::reset()
{
    m_pickState = Unqueued;
    m_layerStates.clear();
    m_pickResults.clear();
}
