/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsstudio3drenderer_p.h"
#include "q3dsstudio3ditem_p.h"
#include "q3dsstudio3dview_p.h"
#include <private/q3dslayer3dsgnode_p.h>
#include <private/q3dsengine_p.h>
#include <private/q3dslogging_p.h>
#include <private/qdragonrenderaspect_p.h>
#include <private/dragongltexture_p.h>
#include <QQuickWindow>
#include <QOpenGLContext>
#include <QGuiApplication>
#include <QLoggingCategory>
#include <Qt3DCore/QAspectEngine>
#include <Qt3DRender/private/qrenderaspect_p.h>
#include <Qt3DRender/private/renderer_p.h>
#include <Qt3DRender/private/nodemanagers_p.h>
#include <Qt3DRender/private/resourceaccessor_p.h>
#include <QOpenGLTexture>
#include <QSGTexture>

QT_BEGIN_NAMESPACE

class ContextSaver
{
public:
    ContextSaver()
    {
        m_context = QOpenGLContext::currentContext();
        m_surface = m_context ? m_context->surface() : nullptr;
    }

    ~ContextSaver()
    {
        if (m_context && m_context->surface() != m_surface)
            m_context->makeCurrent(m_surface);
    }

    QOpenGLContext *context() const { return m_context; }
    QSurface *surface() const { return m_surface; }

private:
    QOpenGLContext *m_context;
    QSurface *m_surface;
};

// There is one renderer object for each Studio3D item. It lives on the Qt
// Quick render thread (which may also be the gui/main thread with the basic
// render loop).

Q3DSStudio3DRenderer::Q3DSStudio3DRenderer(Q3DSStudio3DItem *item, Q3DSLayer3DSGNode *node, Qt3DCore::QAspectEngine *aspectEngine)
    : m_item(item),
      m_node(node), // can be null
      m_aspectEngine(aspectEngine)
{
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    qCDebug(lcStudio3D, "[R] new renderer %p, window is %p, context is %p, aspect engine %p",
            this, m_item->window(), ctx, m_aspectEngine);

    connect(m_item->window(), &QQuickWindow::beforeRendering,
            this, &Q3DSStudio3DRenderer::renderOffscreen, Qt::DirectConnection);

    ContextSaver saver;
    if (m_item->engine()->flags().testFlag(Q3DSEngine::AwakenTheDragon)) {
        m_dragonRenderAspect = new Qt3DRender::Dragon::QDragonRenderAspect(Qt3DRender::Dragon::Renderer::Synchronous);
        m_dragonRenderAspect->initialize(ctx);
        m_aspectEngine->registerAspect(m_dragonRenderAspect);
    } else {
        m_renderAspect = new Qt3DRender::QRenderAspect(Qt3DRender::QRenderAspect::Synchronous);
        m_aspectEngine->registerAspect(m_renderAspect);
        m_renderAspectD = static_cast<Qt3DRender::QRenderAspectPrivate *>(Qt3DRender::QRenderAspectPrivate::get(m_renderAspect));
        m_renderAspectD->renderInitialize(ctx);
    }

    QMetaObject::invokeMethod(m_item, "startEngine");

    m_usingRenderThread = QThread::currentThread() != qGuiApp->thread();
    m_renderTimer.start();
}

Q3DSStudio3DRenderer::~Q3DSStudio3DRenderer()
{
    qCDebug(lcStudio3D, "[R] renderer %p dtor", this);
    ContextSaver saver;

    if (m_renderAspectD != nullptr)
        m_renderAspectD->renderShutdown();

    if (m_dragonRenderAspect != nullptr) {
        m_dragonRenderAspect->beginRenderShutdown();
        m_currentFrame = Qt3DRender::Dragon::Renderer::Frame();
        m_dragonRenderAspect->endRenderShutdown();
    }
}

void Q3DSStudio3DRenderer::invalidateItem()
{
    // must be called from the main thread (or not at all if Quick render thread == main)
    Q_ASSERT(thread() != QThread::currentThread());
    QMutexLocker lock(&m_itemInvalidLock);
    m_itemInvalid = true;
}

static bool sampleCountEquals(int a, int b)
{
    return a == b || (a <= 1 && b <= 1);
}

void Q3DSStudio3DRenderer::renderOffscreen()
{
    // m_item may be destroyed already
    QMutexLocker lock(&m_itemInvalidLock);
    if (m_itemInvalid)
        return;

    QQuickWindow *w = m_item->window();
    if (!w)
        return;

    QSize size = m_item->size().toSize() * w->effectiveDevicePixelRatio();
    if (size.isEmpty()) // can happen when there are views since the app may not bother setting a size then
        size = QSize(64, 64);

    // We always need an FBO and a texture, even when composition is turned off
    // (due to using views), there is no way around this for now.
    if (m_fbo.isNull() || m_fbo->size() != size) {
        m_fbo.reset(new QOpenGLFramebufferObject(size, QOpenGLFramebufferObject::CombinedDepthStencil));
        m_texture.reset(w->createTextureFromId(m_fbo->texture(), m_fbo->size(), QQuickWindow::TextureHasAlphaChannel));
        if (m_node)
            m_node->setTexture(m_texture.data());
    } else if (m_node && m_nodeChanged) { // m_node can change to null and then later back to a valid node, handle this
        m_node->setTexture(m_texture.data());
    }
    m_nodeChanged = false;

    ContextSaver saver;
    w->resetOpenGLState();
    m_fbo->bind();

    if (m_item->engine()->flags().testFlag(Q3DSEngine::AwakenTheDragon))
        m_currentFrame = m_dragonRenderAspect->renderSynchronous(std::move(m_currentFrame));
    else
        m_renderAspectD->renderSynchronous();

    if (saver.context()->surface() != saver.surface())
        saver.context()->makeCurrent(saver.surface());
    w->resetOpenGLState();

    if (m_node) {
        m_node->markDirty(QSGNode::DirtyMaterial);
    } else {
        for (auto vd : qAsConst(m_viewDesc)) {
            GLuint id = 0;
            if (m_item->engine()->flags().testFlag(Q3DSEngine::AwakenTheDragon)) {
                // Unfortunately, we cannot assume that the texture is uploaded,
                // because we might have rendered before the texture was created on the backend
                // This might change if we at some point remove the backend/frontend separation.
                if (!m_currentFrame.uploadedTextures.contains(vd.textureNodeId))
                    continue;
                const auto &glTexture = m_currentFrame.uploadedTextures[vd.textureNodeId];
                id = glTexture->openGLTexture->textureId();
            } else {
                QSharedPointer<Qt3DRender::Render::ResourceAccessor> ra = m_renderAspectD->m_renderer->nodeManagers()->resourceAccessor();
                if (!ra)
                    continue;
                QOpenGLTexture *tex = nullptr;
                ra->accessResource(Qt3DRender::Render::ResourceAccessor::OGLTextureRead, vd.textureNodeId, (void**) &tex, nullptr);
                if (!tex)
                    continue;
                id = tex->textureId();
            }

            Q3DSLayer3DSGNode *viewNode = vd.view->node();
            const QSize viewSizePixels = vd.viewSizeWithoutDpr * vd.dpr;
            const bool viewLayerSizingIsOutOfSync = vd.view->sizeLayerToView()
                    && vd.layerSizePixelsWithoutSsaa != viewSizePixels;
            if (viewNode && !viewLayerSizingIsOutOfSync) {
                if (uint(viewNode->texture()->textureId()) != id
                        || viewNode->texture()->textureSize() != vd.layerSizePixels
                        || !sampleCountEquals(viewNode->sampleCount(), vd.layerSampleCount)
                        || viewNode->rect().size() != vd.viewSizeWithoutDpr)
                {
                    QSGTexture *t = w->createTextureFromId(id, vd.layerSizePixels, QQuickWindow::TextureHasAlphaChannel);
                    t->setFiltering(QSGTexture::Linear);

                    viewNode->setTexture(t, vd.layerSampleCount); // owns, so previous texture is destroyed
                    vd.view->notifyTextureChange(t, vd.layerSampleCount);

                    viewNode->setRect(QRectF(QPointF(0, 0), vd.viewSizeWithoutDpr)); // logical size (no dpr)
                }

                viewNode->markDirty(QSGNode::DirtyMaterial);
            }
        }
    }

    if (m_item->engine())
        m_item->engine()->reportQuickRenderLoopStats(m_renderTimer.restart(), m_usingRenderThread);

    // ### hmm...
    w->update();
}

QT_END_NAMESPACE
