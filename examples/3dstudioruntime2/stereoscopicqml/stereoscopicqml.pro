TEMPLATE = app

QT += widgets qml quick 3dstudioruntime2

SOURCES += \
    main.cpp

RESOURCES += stereoscopicqml.qrc

OTHER_FILES += \
    main.qml \
    SettingsPanel.qml

target.path = $$[QT_INSTALL_EXAMPLES]/3dstudioruntime2/$$TARGET
INSTALLS += target
