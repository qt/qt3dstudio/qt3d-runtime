/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.8
import QtStudio3D 2.2
import QtQuick.Controls 2.2

Rectangle {
    id: root
    color: "lightGray"

    Button {
        id: profBtn
        text: "Profile UI"
        // we use a Studio3DProfiler item so toggle that instead of pres.profileUiVisible
        onClicked: quickBasedProfileUI.visible = !quickBasedProfileUI.visible
        focusPolicy: Qt.NoFocus
    }

    CheckBox {
        id: shaderEffectCb
        text: "ShaderEffect"
        checked: true
        anchors.left: profBtn.right
        focusPolicy: Qt.NoFocus
    }

    CheckBox {
        id: sizeFollowCb
        text: "Layer follows View size"
        checked: true
        anchors.left: shaderEffectCb.right
        focusPolicy: Qt.NoFocus
    }

    CheckBox {
        id: msaaCb
        text: "SSAA/MSAA version"
        checked: false
        anchors.left: sizeFollowCb.right
        focusPolicy: Qt.NoFocus
    }

    Studio3D {
        id: s3d
        anchors.fill: parent
        anchors.margins: 40

        Presentation {
            id: pres
            source: msaaCb.checked ? "qrc:/layers_ssaa_msaa.uip" : "qrc:/layers.uip"
            profilingEnabled: true
        }
    }

    Rectangle {
        y: 40
        width: parent.width / 2
        height: parent.height - 40
        color: "red"

        View3D {
            id: viewFor3DLayer1
            engine: s3d
            source: "LayerWithTransparentBackground"
            sizeLayerToView: sizeFollowCb.checked

            anchors.fill: parent
            anchors.margins: 10

            property bool shaderEffectActive: shaderEffectCb.checked
            visible: !shaderEffectActive
        }

        ShaderEffectSource {
            id: effSource
            sourceItem: viewFor3DLayer1
            visible: viewFor3DLayer1.shaderEffectActive
        }

        ShaderEffect {
            visible: viewFor3DLayer1.shaderEffectActive
            anchors.fill: parent
            anchors.margins: 10
            property variant source: effSource
            property real amplitude: 0.02
            property real frequency: 20
            property real time: 0
            NumberAnimation on time { loops: Animation.Infinite; from: 0; to: Math.PI * 2; duration: 600 }
            fragmentShader: GraphicsInfo.profile === GraphicsInfo.OpenGLCoreProfile ?
                "#version 330 core\n" +
                "uniform float qt_Opacity;\n" +
                "uniform float amplitude;\n" +
                "uniform float frequency;\n" +
                "uniform float time;\n" +
                "uniform sampler2D source;\n" +
                "in vec2 qt_TexCoord0;\n" +
                "out vec4 fragColor;\n" +
                "void main() {\n" +
                "    vec2 p = sin(time + frequency * qt_TexCoord0);\n" +
                "    fragColor = texture(source, qt_TexCoord0 + amplitude * vec2(p.y, -p.x)) * qt_Opacity;\n" +
                "}"
                              :
                "uniform float qt_Opacity;\n" +
                "uniform float amplitude;\n" +
                "uniform float frequency;\n" +
                "uniform float time;\n" +
                "uniform sampler2D source;\n" +
                "varying vec2 qt_TexCoord0;\n" +
                "void main() {\n" +
                "    vec2 p = sin(time + frequency * qt_TexCoord0);\n" +
                "    gl_FragColor = texture2D(source, qt_TexCoord0 + amplitude * vec2(p.y, -p.x)) * qt_Opacity;\n" +
                "}"
        }

        Text {
            text: "This here is a View3D in a Rectangle\ndisplaying only one layer from the Qt 3D Studio scene"
            color: "black"
            font.pointSize: 16
        }
    }

    Rectangle {
        x: parent.width / 2
        y: 40
        width: parent.width / 2
        height: parent.height - 40
        color: "green"

        View3D {
            id: viewFor3DLayer2
            engine: s3d
            source: "LayerWithSolidBackground"
            sizeLayerToView: sizeFollowCb.checked

            anchors.fill: parent
            anchors.margins: 10

            transform: [
                Rotation { id: rotation; axis.x: 0; axis.z: 0; axis.y: 1; angle: 0; origin.x: viewFor3DLayer2.width / 2; origin.y: viewFor3DLayer2.height / 2; },
                Translate { id: txOut; x: -viewFor3DLayer2.width / 2; y: -viewFor3DLayer2.height / 2 },
                Scale { id: scale; },
                Translate { id: txIn; x: viewFor3DLayer2.width / 2; y: viewFor3DLayer2.height / 2 }
            ]

            SequentialAnimation {
                PauseAnimation { duration: 2000 }
                ParallelAnimation {
                    NumberAnimation { target: scale; property: "xScale"; to: 0.6; duration: 1000; easing.type: Easing.InOutBack }
                    NumberAnimation { target: scale; property: "yScale"; to: 0.6; duration: 1000; easing.type: Easing.InOutBack }
                }
                NumberAnimation { target: rotation; property: "angle"; to: 80; duration: 1000; easing.type: Easing.InOutCubic }
                NumberAnimation { target: rotation; property: "angle"; to: -80; duration: 1000; easing.type: Easing.InOutCubic }
                NumberAnimation { target: rotation; property: "angle"; to: 0; duration: 1000; easing.type: Easing.InOutCubic }
                NumberAnimation { target: viewFor3DLayer2; property: "opacity"; to: 0.5; duration: 1000; easing.type: Easing.InOutCubic }
                PauseAnimation { duration: 1000 }
                NumberAnimation { target: viewFor3DLayer2; property: "opacity"; to: 0.8; duration: 1000; easing.type: Easing.InOutCubic }
                ParallelAnimation {
                    NumberAnimation { target: scale; property: "xScale"; to: 1; duration: 1000; easing.type: Easing.InOutBack }
                    NumberAnimation { target: scale; property: "yScale"; to: 1; duration: 1000; easing.type: Easing.InOutBack }
                }
                running: true
                loops: Animation.Infinite
            }
        }

        Text {
            text: "Another View3D showing the other layer"
            color: "black"
            font.pointSize: 16
        }
    }

    // This example uses Qt Quick-based debug views because rendering them with
    // Qt 3D is not feasible anymore in the separated views model (as Qt 3D is
    // not responsible for the final composed image).
    Studio3DProfiler {
        id: quickBasedProfileUI
        visible: false
        focus: true
        anchors.fill: parent
        anchors.topMargin: 40 // make sure we do not cover the top area with the buttons
    }
}
