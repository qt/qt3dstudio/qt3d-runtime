/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QGuiApplication>
#include <QQmlContext>
#include <QQuickView>
#include <QTime>
#include <QTimer>
#include <Q3DSPresentation>

#include <private/q3dsengine_p.h>
#include <private/q3dsuippresentation_p.h>

class SceneManipulator : public QObject
{
    Q_OBJECT

public slots:
    void doIt(Q3DSPresentation *pres);

private:
    QTimer m_timer;
    int m_dynCount = 0;
};

void SceneManipulator::doIt(Q3DSPresentation *pres)
{
    Q3DSEngine *engine = pres->engine();
    Q3DSUipPresentation *presentation = engine->presentation();
    Q3DSSlide *slide1 = static_cast<Q3DSSlide *>(presentation->masterSlide()->firstChild());

    Q3DSModelNode *prim = presentation->newObject<Q3DSModelNode>(QByteArrayLiteral("dynmodel") + QByteArray::number(m_dynCount));
    switch (qrand() % 4) {
    case 0:
        prim->setMesh(QLatin1String("#Cube"));
        break;
    case 1:
        prim->setMesh(QLatin1String("#Sphere"));
        break;
    case 2:
        prim->setMesh(QLatin1String("#Cylinder"));
        break;
    case 3:
        prim->setMesh(QLatin1String("#Cone"));
        break;
    }

    Q3DSDefaultMaterial *primMat = presentation->newObject<Q3DSDefaultMaterial>(QByteArrayLiteral("dynmat") + QByteArray::number(m_dynCount));
    prim->appendChildNode(primMat);

    prim->setPosition(QVector3D((qrand() % 600) - 300, (qrand() % 600) - 300, 0));
    prim->setRotation(QVector3D(30, 45, 0));
    const float scale = ((qrand() % 100) + 1) / 100.0f;
    prim->setScale(QVector3D(scale, scale, scale));

    slide1->addObject(primMat);
    slide1->addObject(prim);

    ++m_dynCount;

    // go live
    presentation->objectByName<Q3DSLayerNode>(QLatin1String("Layer"))->appendChildNode(prim);

    // Animations cannot be managed dynamically for now. However, nothing
    // prevents us from changing properties as we see fit:
    m_timer.setInterval(30);
    connect(&m_timer, &QTimer::timeout, this, [prim] {
        QVector3D v = prim->position();
        v.setX(v.x() - 1);
        QVector3D r = prim->rotation();
        r.setY(r.y() + 1);
        prim->notifyPropertyChanges({
                                        prim->setPosition(v),
                                        prim->setRotation(r)
                                    });
    });
    m_timer.start();
}

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qputenv("QSG_INFO", "1");
    QGuiApplication app(argc, argv);

    qsrand(QTime::currentTime().msec());

    // Use the ideal format (i.e. OpenGL version and profile) recommended by
    // the Qt 3D Studio runtime. Without this the format set on the QQuickView
    // would be used instead.
    QSurfaceFormat::setDefaultFormat(Q3DS::surfaceFormat());

    QQuickView viewer;

    SceneManipulator manip;
    viewer.rootContext()->setContextProperty("sceneManipulator", &manip);

    viewer.setSource(QUrl("qrc:/main.qml"));

    viewer.setTitle(QStringLiteral("Qt 3D Studio Example"));
    viewer.setResizeMode(QQuickView::SizeRootObjectToView);
    viewer.resize(1280, 720);
    viewer.show();

    return app.exec();
}

#include "main.moc"
