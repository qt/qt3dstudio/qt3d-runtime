TARGET = qml3dapi

QT += quick 3dstudioruntime2

SOURCES += \
    main.cpp

RESOURCES += qml3dapi.qrc

OTHER_FILES += \
    main.qml
