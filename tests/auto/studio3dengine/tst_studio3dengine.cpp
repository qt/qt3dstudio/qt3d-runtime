/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the test suite of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL-EXCEPT$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtTest/qtest.h>
#include <QtTest/qsignalspy.h>

#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>
#include <QtCore/qurl.h>

#include <private/q3dsuippresentation_p.h>
#include <private/q3dslayer3d_p.h>
#include <private/q3dsgroup3d_p.h>

#include "../shared/shared.h"

class tst_Studio3DEngine : public QObject
{
    Q_OBJECT

public:
    tst_Studio3DEngine();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testSimple();
    void testReparent();
    void testReparentSubTree();
    void testParentingOrder();
    void testEngineChangeToNullAndBack();
    void testLayerParentNullAndBack();
};

tst_Studio3DEngine::tst_Studio3DEngine()
{
}

void tst_Studio3DEngine::initTestCase()
{
    if (!isOpenGLGoodEnough())
        QSKIP("This platform does not support OpenGL proper");

    QSurfaceFormat::setDefaultFormat(Q3DS::surfaceFormat());

    // do not bother disabling dialogs, Studio3DEngine is expected to avoid those implicitly
}

void tst_Studio3DEngine::cleanupTestCase()
{
}

void tst_Studio3DEngine::testSimple()
{
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl(QLatin1String("qrc:/data/simple.qml")));

    QVERIFY(view.rootObject());
    QVERIFY(view.rootObject()->childItems().count() > 0);

    QQuickItem *s3d = view.rootObject()->childItems()[0];
    QVERIFY(s3d);
    const QMetaObject *s3dMeta = s3d->metaObject();
    QVERIFY(!strcmp(s3dMeta->className(), "Q3DSStudio3DEngine"));

    view.resize(600, 500);
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));

    if (view.rendererInterface()->graphicsApi() != QSGRendererInterface::OpenGL)
        QSKIP("This test needs Qt Quick on OpenGL");

    Q3DSLayer3D *layer3d = qobject_cast<Q3DSLayer3D *>(view.rootObject()->childItems()[1]);
    QVERIFY(layer3d);
    QCOMPARE(layer3d->engine(), s3d);

    Q3DSLayerNode *layer3DS = layer3d->layerNode();
    QVERIFY(layer3DS);
    QCOMPARE(layer3DS->explicitSize(), layer3d->size() * view.effectiveDevicePixelRatio());

    QCOMPARE(layer3d->childItems().count(), 1);
    Q3DSGroup3D *group3d_1 = qobject_cast<Q3DSGroup3D *>(layer3d->childItems()[0]);
    QVERIFY(group3d_1);
    QCOMPARE(group3d_1->layer(), layer3d);

    QCOMPARE(group3d_1->object()->type(), Q3DSGraphObject::Group);
    Q3DSGroupNode *group3DS_1 = static_cast<Q3DSGroupNode *>(group3d_1->object());
    QVERIFY(group3DS_1);

    QCOMPARE(group3d_1->childItems().count(), 1);
    Q3DSGroup3D *group3d_2 = qobject_cast<Q3DSGroup3D *>(group3d_1->childItems()[0]);
    QVERIFY(group3d_2);
    QCOMPARE(group3d_2->layer(), layer3d);

    QCOMPARE(group3d_2->object()->type(), Q3DSGraphObject::Group);
    Q3DSGroupNode *group3DS_2 = static_cast<Q3DSGroupNode *>(group3d_2->object());
    QVERIFY(group3DS_2);

    QCOMPARE(group3DS_2->parent(), group3DS_1);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(layer3DS->parent(), layer3d->presentation()->scene());

    QVERIFY(layer3d->slide()->parent() == layer3d->presentation()->masterSlide());
    QVERIFY(!layer3d->slide()->nextSibling());
    const QSet<Q3DSGraphObject *> slideObjects = layer3d->slide()->objects();
    QCOMPARE(slideObjects.count(), 3);
}

void tst_Studio3DEngine::testReparent()
{
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl(QLatin1String("qrc:/data/simple.qml")));

    view.resize(600, 500);
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));

    if (view.rendererInterface()->graphicsApi() != QSGRendererInterface::OpenGL)
        QSKIP("This test needs Qt Quick on OpenGL");

    Q3DSLayer3D *layer3d = qobject_cast<Q3DSLayer3D *>(view.rootObject()->childItems()[1]);
    Q3DSLayerNode *layer3DS = layer3d->layerNode();
    Q3DSGroup3D *group3d_1 = qobject_cast<Q3DSGroup3D *>(layer3d->childItems()[0]);
    Q3DSGroupNode *group3DS_1 = static_cast<Q3DSGroupNode *>(group3d_1->object());
    Q3DSGroup3D *group3d_2 = qobject_cast<Q3DSGroup3D *>(group3d_1->childItems()[0]);
    Q3DSGroupNode *group3DS_2 = static_cast<Q3DSGroupNode *>(group3d_2->object());

    // Layer(Group1(Group2))
    QCOMPARE(group3DS_2->parent(), group3DS_1);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(layer3d->slide()->objects().count(), 3);

    // Layer(Group1 Group2)
    group3d_2->setParentItem(layer3d);
    QCOMPARE(group3DS_2->parent(), layer3DS);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(group3DS_1->nextSibling(), group3DS_2);
    QCOMPARE(layer3d->slide()->objects().count(), 3);

    // Layer(Group2 Group1)
    group3d_2->setParentItem(nullptr);
    QCOMPARE(group3DS_2->parent(), nullptr);
    QCOMPARE(group3d_2->layer(), nullptr);
    group3d_1->setParentItem(nullptr);
    QCOMPARE(group3DS_1->parent(), nullptr);
    QCOMPARE(group3d_1->layer(), nullptr);
    group3d_2->setParentItem(layer3d);
    QCOMPARE(group3DS_2->parent(), layer3DS);
    QCOMPARE(group3DS_2->nextSibling(), nullptr);
    group3d_1->setParentItem(layer3d);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(group3d_1->layer(), layer3d);
    QCOMPARE(group3DS_2->nextSibling(), group3DS_1);
    QCOMPARE(group3d_2->layer(), layer3d);
    QCOMPARE(layer3d->slide()->objects().count(), 3);

    // Layer(Group1) Group2
    group3d_2->setParentItem(nullptr);
    QCOMPARE(group3d_2->object(), group3DS_2);
    QCOMPARE(group3DS_2->parent(), nullptr);
    QCOMPARE(group3DS_2->nextSibling(), nullptr);
    QCOMPARE(group3d_2->layer(), nullptr);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(group3d_1->layer(), layer3d);
    QCOMPARE(layer3d->slide()->objects().count(), 2);

    // Layer(Group1(Group2))
    group3d_2->setParentItem(group3d_1);
    QCOMPARE(group3d_2->object(), group3DS_2);
    QCOMPARE(group3DS_2->parent(), group3DS_1);
    QCOMPARE(group3d_2->layer(), layer3d);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(group3d_1->layer(), layer3d);
    QCOMPARE(layer3d->slide()->objects().count(), 3);
}

void tst_Studio3DEngine::testReparentSubTree()
{
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl(QLatin1String("qrc:/data/simple.qml")));

    view.resize(600, 500);
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));

    if (view.rendererInterface()->graphicsApi() != QSGRendererInterface::OpenGL)
        QSKIP("This test needs Qt Quick on OpenGL");

    Q3DSLayer3D *layer3d = qobject_cast<Q3DSLayer3D *>(view.rootObject()->childItems()[1]);
    Q3DSLayerNode *layer3DS = layer3d->layerNode();
    Q3DSGroup3D *group3d_1 = qobject_cast<Q3DSGroup3D *>(layer3d->childItems()[0]);
    Q3DSGroupNode *group3DS_1 = static_cast<Q3DSGroupNode *>(group3d_1->object());
    Q3DSGroup3D *group3d_2 = qobject_cast<Q3DSGroup3D *>(group3d_1->childItems()[0]);
    Q3DSGroupNode *group3DS_2 = static_cast<Q3DSGroupNode *>(group3d_2->object());

    // Layer(Group1(Group2))
    QCOMPARE(group3DS_2->parent(), group3DS_1);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(layer3d->slide()->objects().count(), 3);

    // Layer Group1(Group2)
    group3d_1->setParentItem(nullptr);
    QCOMPARE(group3DS_1->parent(), nullptr);
    QCOMPARE(group3d_1->layer(), nullptr);
    QCOMPARE(group3DS_2->parent(), group3DS_1);
    QCOMPARE(group3d_2->layer(), nullptr);
    QCOMPARE(layer3d->slide()->objects().count(), 1);

    // Layer Group1(Group2(Group3))
    // Group3 not activated at this point since the tree it gets added to is not live
    Q3DSGroup3D *group3d_3 = new Q3DSGroup3D;
    group3d_3->setParentItem(group3d_2);
    QCOMPARE(group3d_3->layer(), nullptr);
    QCOMPARE(group3d_3->object(), nullptr);
    QCOMPARE(layer3d->slide()->objects().count(), 1);

    // Layer(Group1(Group2(Group3)))
    group3d_1->setParentItem(layer3d);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(group3DS_2->parent(), group3DS_1);
    QCOMPARE(group3d_3->object()->parent(), group3DS_2);
    QCOMPARE(group3d_3->layer(), layer3d);

    Q3DSGroupNode *group3DS_3 = group3d_3->groupNode();
    QVERIFY(layer3d->slide()->objects().contains(group3DS_3));
    QCOMPARE(layer3d->slide()->objects().count(), 4);

    // Layer(Group1 Group2(Group3))
    group3d_2->setParentItem(nullptr);
    QCOMPARE(group3d_2->object(), group3DS_2);
    QCOMPARE(group3DS_2->parent(), nullptr);
    QCOMPARE(group3d_2->layer(), nullptr);
    QCOMPARE(group3d_3->object(), group3DS_3);
    QCOMPARE(group3DS_3->parent(), group3DS_2);
    QCOMPARE(group3d_3->layer(), nullptr);

    // Layer(Group1(Group2(Group3))
    group3d_2->setParentItem(group3d_1);
    QCOMPARE(group3d_2->object(), group3DS_2);
    QCOMPARE(group3DS_2->parent(), group3DS_1);
    QCOMPARE(group3d_2->layer(), layer3d);
    QCOMPARE(group3d_3->object(), group3DS_3);
    QCOMPARE(group3DS_3->parent(), group3DS_2);
    QCOMPARE(group3d_3->layer(), layer3d);
}

void tst_Studio3DEngine::testParentingOrder()
{
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl(QLatin1String("qrc:/data/simple.qml")));

    view.resize(600, 500);
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));

    if (view.rendererInterface()->graphicsApi() != QSGRendererInterface::OpenGL)
        QSKIP("This test needs Qt Quick on OpenGL");

    Q3DSLayer3D *layer3d = qobject_cast<Q3DSLayer3D *>(view.rootObject()->childItems()[1]);
    Q3DSLayerNode *layer3DS = layer3d->layerNode();
    Q3DSGroup3D *group3d_1 = qobject_cast<Q3DSGroup3D *>(layer3d->childItems()[0]);
    Q3DSGroupNode *group3DS_1 = static_cast<Q3DSGroupNode *>(group3d_1->object());
    Q3DSGroup3D *group3d_2 = qobject_cast<Q3DSGroup3D *>(group3d_1->childItems()[0]);
    Q3DSGroupNode *group3DS_2 = static_cast<Q3DSGroupNode *>(group3d_2->object());

    // Layer(Group1(Group2))
    QCOMPARE(group3DS_2->parent(), group3DS_1);
    QCOMPARE(group3DS_1->parent(), layer3DS);
    QCOMPARE(layer3d->slide()->objects().count(), 3);
    QCOMPARE(group3d_1->layer(), layer3d);
    QCOMPARE(group3d_2->layer(), layer3d);

    // Layer Group1 Group2
    group3d_1->setParentItem(nullptr);
    QVERIFY(!group3DS_1->parent());
    QVERIFY(!group3DS_1->nextSibling());
    QVERIFY(!group3d_1->layer());
    group3d_2->setParentItem(nullptr);
    QVERIFY(!group3DS_2->parent());
    QVERIFY(!group3DS_2->nextSibling());
    QVERIFY(!group3d_2->layer());

    // Layer Group1(Group4 Group3 Group5) Group2
    Q3DSGroup3D *group3d_3 = new Q3DSGroup3D;
    Q3DSGroup3D *group3d_4 = new Q3DSGroup3D;
    Q3DSGroup3D *group3d_5 = new Q3DSGroup3D;
    group3d_4->setParentItem(group3d_1);
    QVERIFY(!group3d_4->object());
    group3d_3->setParentItem(group3d_1);
    QVERIFY(!group3d_3->object());
    group3d_5->setParentItem(group3d_1);
    QVERIFY(!group3d_5->object());

    // Layer(Group1(Group4 Group3 Group5)) Group2
    group3d_1->setParentItem(layer3d);
    Q3DSGroupNode *group3DS_4 = group3d_4->groupNode();
    Q3DSGroupNode *group3DS_3 = group3d_3->groupNode();
    Q3DSGroupNode *group3DS_5 = group3d_5->groupNode();
    QVERIFY(group3DS_4);
    QVERIFY(group3DS_3);
    QVERIFY(group3DS_5);
    QCOMPARE(group3d_4->layer(), layer3d);
    QCOMPARE(group3d_3->layer(), layer3d);
    QCOMPARE(group3d_5->layer(), layer3d);
    QCOMPARE(group3DS_4->parent(), group3DS_1);
    QCOMPARE(group3DS_3->parent(), group3DS_1);
    QCOMPARE(group3DS_5->parent(), group3DS_1);
    QCOMPARE(group3DS_4->nextSibling(), group3DS_3);
    QCOMPARE(group3DS_3->nextSibling(), group3DS_5);
    QCOMPARE(group3DS_5->nextSibling(), nullptr);
    QCOMPARE(group3DS_1->childCount(), 3);
    QCOMPARE(layer3d->slide()->objects().count(), 5);

    // Layer(Group1(Group4 Group3 Group5)) Group2(Group6 Group7(Group8))
    Q3DSGroup3D *group3d_6 = new Q3DSGroup3D;
    Q3DSGroup3D *group3d_7 = new Q3DSGroup3D;
    Q3DSGroup3D *group3d_8 = new Q3DSGroup3D;
    group3d_6->setParentItem(group3d_2);
    QVERIFY(!group3d_6->object());
    group3d_7->setParentItem(group3d_2);
    QVERIFY(!group3d_7->object());
    group3d_8->setParentItem(group3d_7);
    QVERIFY(!group3d_8->object());

    // Layer(Group1(Group4 Group3 Group5) Group2(Group6 Group7(Group8)))
    group3d_2->setParentItem(layer3d);
    Q3DSGroupNode *group3DS_6 = group3d_6->groupNode();
    Q3DSGroupNode *group3DS_7 = group3d_7->groupNode();
    Q3DSGroupNode *group3DS_8 = group3d_8->groupNode();
    QVERIFY(group3DS_6);
    QVERIFY(group3DS_7);
    QVERIFY(group3DS_8);
    QCOMPARE(group3d_6->layer(), layer3d);
    QCOMPARE(group3d_7->layer(), layer3d);
    QCOMPARE(group3d_8->layer(), layer3d);
    QCOMPARE(group3DS_2->parent(), layer3DS);
    QCOMPARE(group3DS_1->nextSibling(), group3DS_2);
    QCOMPARE(group3DS_2->nextSibling(), nullptr);
    QCOMPARE(group3DS_2->previousSibling(), group3DS_1);
    QCOMPARE(layer3d->slide()->objects().count(), 9);
    QCOMPARE(group3DS_6->parent(), group3DS_2);
    QCOMPARE(group3DS_7->parent(), group3DS_2);
    QCOMPARE(group3DS_8->parent(), group3DS_7);
    QCOMPARE(group3DS_6->nextSibling(), group3DS_7);
    QCOMPARE(group3DS_7->nextSibling(), nullptr);
    QCOMPARE(group3DS_8->nextSibling(), nullptr);
}

void tst_Studio3DEngine::testEngineChangeToNullAndBack()
{
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl(QLatin1String("qrc:/data/simple.qml")));

    view.resize(600, 500);
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));

    if (view.rendererInterface()->graphicsApi() != QSGRendererInterface::OpenGL)
        QSKIP("This test needs Qt Quick on OpenGL");

    QQuickItem *s3d = view.rootObject()->childItems()[0];
    Q3DSLayer3D *layer3d = qobject_cast<Q3DSLayer3D *>(view.rootObject()->childItems()[1]);
    Q3DSLayerNode *layer3DS = layer3d->layerNode();

    // nulling out the engine is valid, it leads to nulling the presentation
    // but keeping all underlying objects alive [NB the engine cannot actually
    // change to another Studio3DEngine since object IDs are not transferred
    // between presentations]

    QCOMPARE(layer3DS->parent(), layer3d->presentation()->scene());
    layer3d->setEngine(nullptr);
    QCOMPARE(layer3d->layerNode(), layer3DS);
    QCOMPARE(layer3DS->parent(), nullptr);
    QCOMPARE(layer3d->presentation(), nullptr);

    layer3d->setEngine(s3d);
    QCOMPARE(layer3d->layerNode(), layer3DS);
    QCOMPARE(layer3DS->parent(), layer3d->presentation()->scene());
}

void tst_Studio3DEngine::testLayerParentNullAndBack()
{
    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl(QLatin1String("qrc:/data/simple.qml")));

    view.resize(600, 500);
    view.show();
    QVERIFY(QTest::qWaitForWindowExposed(&view));

    if (view.rendererInterface()->graphicsApi() != QSGRendererInterface::OpenGL)
        QSKIP("This test needs Qt Quick on OpenGL");

    Q3DSLayer3D *layer3d = qobject_cast<Q3DSLayer3D *>(view.rootObject()->childItems()[1]);
    Q3DSLayerNode *layer3DS = layer3d->layerNode();

    QQuickItem *layerParent = layer3d->parentItem();
    layer3d->setParentItem(nullptr);
    QCOMPARE(layer3d->layerNode(), layer3DS);
    QVERIFY(layer3d->presentation());

    layer3d->setParentItem(layerParent);
    QCOMPARE(layer3d->layerNode(), layer3DS);
}

QTEST_MAIN(tst_Studio3DEngine)

#include "tst_studio3dengine.moc"
