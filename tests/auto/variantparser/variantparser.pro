TARGET = tst_variantparser
CONFIG += testcase

QT += testlib 3drender 3dstudioruntime2-private

SOURCES += tst_q3dsvariantparser.cpp

RESOURCES += variantparser.qrc
